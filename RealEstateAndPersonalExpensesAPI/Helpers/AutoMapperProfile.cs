﻿using AutoMapper;
using Data.Users;
using RealEstateAndPersonalExpensesAPI.Models.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RealEstateAndPersonalExpensesAPI.Helpers
{
    public class AutoMapperProfile:Profile
    {
        public AutoMapperProfile()
        {
            //CreateMap<User, UserModel>();
            CreateMap<RegisterModel, User>();
            //CreateMap<UpdateModel, User>();
        }
    }
}
