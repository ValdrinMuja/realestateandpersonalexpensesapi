﻿using Data.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace Repository.Users.Interfaces
{
    public interface IUserRepository
    {
        User Create(User user);
    }
}
