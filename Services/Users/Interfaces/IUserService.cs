﻿using Data.Users;
using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Users.Interfaces
{
    public interface IUserService
    {
        User Create(User user, string password);
    }
}
